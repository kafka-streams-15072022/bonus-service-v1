package com.example.bonusservice.controller;

import com.example.bonusservice.service.BonusService;
import com.example.transactionservice.model.events.TransactionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionEventConsumerController {

    private final BonusService bonusService;

    @Autowired
    public TransactionEventConsumerController(BonusService bonusService) {
        this.bonusService = bonusService;
    }

    @PutMapping(path = "events/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> executeTransaction(@RequestBody TransactionEvent transactionEvent) {
        bonusService.onTransactionEvent(transactionEvent);
        return ResponseEntity.noContent().build();
    }
}
