package com.example.bonusservice.controller;

import com.example.bonusservice.model.dto.BonusAccountDto;
import com.example.bonusservice.service.BonusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
public class BonusAccountController {

    private final BonusService bonusService;

    @Autowired
    public BonusAccountController(BonusService bonusService) {
        this.bonusService = bonusService;
    }

    @GetMapping(path = "accounts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BonusAccountDto getAccount(@PathVariable("id")UUID id) {
        return bonusService.findAccount(id).map(account -> {
            final var accountDto = new BonusAccountDto();
            accountDto.setLevel(account.getLevel());
            accountDto.setValue(account.getValue());
            accountDto.setLastUpdate(account.getLastUpdateTimestamp());
            return accountDto;
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
