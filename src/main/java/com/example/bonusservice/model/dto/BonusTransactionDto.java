package com.example.bonusservice.model.dto;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class BonusTransactionDto {

    private UUID from;
    private BigDecimal amount;

    public UUID getFrom() {
        return from;
    }

    public void setFrom(UUID from) {
        this.from = from;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusTransactionDto that = (BonusTransactionDto) o;
        return Objects.equals(from, that.from) && Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, amount);
    }
}
